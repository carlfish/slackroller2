{ mkDerivation, aeson, attoparsec, base, http-api-data, mtl
, network, random, read-env-var, servant, servant-server
, socket-activation, stdenv, text, wai, wai-extra, warp
}:
mkDerivation {
  pname = "slackroller2";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson attoparsec base http-api-data mtl network random read-env-var
    servant servant-server socket-activation text wai wai-extra warp
  ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base ];
  homepage = "https://bitbucket.org/carlfish/slackroller2";
  license = stdenv.lib.licenses.bsd3;
}
