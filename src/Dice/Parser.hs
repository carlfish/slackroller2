module Dice.Parser (parse) where

import Control.Applicative ((<*>),
                            (*>),
                            (<*),
                            (<$>),
                            (<|>),
                            pure)
import qualified Data.Attoparsec.Text as A
import Data.Attoparsec.Text (Parser)
import Data.Text (Text)
import Dice.Types

parse :: Text -> Either String [ CompoundRoll ]
parse =
  A.parseOnly (ws *> compoundRolls <* ws <* A.endOfInput)

compoundRolls :: Parser [ CompoundRoll ]
compoundRolls =
  commaSeparated compoundRoll

compoundRoll :: Parser CompoundRoll
compoundRoll =
  let
    repeat' = Times <$> (countingNumber <* ws <* ("x" <|> "times"))
    oldRepeat = (flip CompoundRoll) <$> roll <* ws <*> repeat'
    newRepeat = CompoundRoll <$> (A.option (Times 1) repeat') <* ws <*> roll
  in
    oldRepeat <|> newRepeat

roll :: Parser Roll
roll =
  let
    first = step
    rest = (,) <$> (ws *> plusOrMinus <* ws) <*> step
  in
    Roll <$> first <*> (A.many' rest)

step :: Parser Step
step =
  namedStep <|> dice <|> constant

namedStep :: Parser Step
namedStep =
  let
    twod20 kd = pure (Dice (Times 2) (Sides 20) kd)
    adv = twod20 (Keep Highest 1) <* ("advantage" <|> "adv")
    dis = twod20 (Keep Lowest 1) <* ("disadvantage" <|> "dis")
  in
    adv <|> dis

constant :: Parser Step
constant =
  Constant <$> countingNumber

dice :: Parser Step
dice =
  let
    count = Times <$> (A.option 1 countingNumber)
    sides = Sides <$> (countingNumber <|> percent)
  in
    Dice <$> count <* "d" <*> sides <* ws <*> dropKeep

dropKeep :: Parser DropKeep
dropKeep =
  let
    dk f ps = f <$> ((A.choice ps) *> ws *> (A.option 1 countingNumber))
    kh = dk (Keep Highest) ["keep" *> ws *> "highest", "keep", "kh", "k"]
    kl = dk (Keep Lowest) ["keep" *> ws *> "lowest", "kl"]
    dl = dk (Drop Lowest) ["drop" *> ws *> "lowest", "drop", "dl", "d"]
    dh = dk (Drop Highest) ["drop" *> ws *> "highest", "dh"]
  in
    -- Order is important as "d/drop" and "k/keep" patterns are greedy
    kl <|> dh <|> kh <|> dl <|> pure KeepAll

commaSeparated :: Parser a -> Parser [ a ]
commaSeparated p =
  A.sepBy p (ws <* (A.char ',') <* ws)

percent :: Parser Int
percent =
  pure 100 <* "00"

countingNumber :: Parser Int
countingNumber =
  (<$>) read $ ((:)) <$> digitNotZero <*> A.many' digit

digitNotZero :: Parser Char
digitNotZero =
  A.satisfy (\c -> c > '0' && c <= '9')

digit :: Parser Char
digit =
  A.satisfy (\c -> c >= '0' && c <= '9')

plusOrMinus :: Parser Modifier
plusOrMinus = (pure Plus <* A.char '+') <|> (pure Minus <* A.char '-')

ws :: Parser ()
ws =
  A.skipSpace
