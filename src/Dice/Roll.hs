{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}

module Dice.Roll
  ( rollAll
  , roll
  ) where

import Control.Monad.Reader (ReaderT, runReaderT, ask)
import Control.Monad.Trans (lift)
import Data.List (sort)
import Data.Tuple (swap)
import Dice.Types

rollAll :: Monad m => (Int -> m Int) -> [ Roll ] -> m [ RollResult ]
rollAll f rs =
  traverse (roll f) rs

roll :: Monad m => (Int -> m Int) -> Roll -> m RollResult
roll f r =
  runReaderT (roll' r) f

type Rolled a = forall m. Monad m => ReaderT (Int -> m Int) m a

roll' :: Roll -> Rolled RollResult
roll' (Roll s ms) =
  let
    rollMStep (m, ss) = ((,) m) <$> rollStep ss
  in
    RollResult <$> rollStep s <*> traverse rollMStep ms

rollStep :: Step -> Rolled StepResult
rollStep st@(Constant c) =
  pure $ StepResult st [] [c]
rollStep st@(Dice (Times t) (Sides s) dk) =
  let
    dice = replicate t s
    toResult rs = let (e, i) = partition dk rs in StepResult st e i
  in
    toResult <$> traverse rollOne dice

rollOne :: Int -> Rolled Int
rollOne sides =
  ask >>= (\roller -> lift (roller sides))

partition :: Ord a => DropKeep -> [a] -> ([a], [a])
partition KeepAll          = (,) []
partition (Keep Lowest n)  =  swap . splitAt n           . sort
partition (Keep Highest n) =  swap . splitAt n . reverse . sort
partition (Drop Lowest n)  =         splitAt n           . sort
partition (Drop Highest n) =         splitAt n . reverse . sort
