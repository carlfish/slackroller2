module Dice.Types
  ( Limits(..)
  , RollResult(..)
  , StepResult(..)
  , CompoundRoll(..)
  , Roll(..)
  , Step(..)
  , Modifier(..)
  , DropKeep(..)
  , HighLow(..)
  , Times(..)
  , Sides(..)
  ) where

import Data.Monoid ((<>))
import Data.List (intercalate)
import Control.Monad (join)

data Limits
  = Limits
  { rolls :: Int
  , dicePerRoll :: Int
  , sidesPerDie :: Int }

data RollResult =
  RollResult StepResult [ (Modifier, StepResult) ]

instance Show RollResult where
  show (RollResult r []) =
    show r
  show (RollResult r mrs) =
    let
      sr (Plus, rr) = " + " <> (show rr)
      sr (Minus, rr) = " - " <> (show rr)
    in
      (show r) <> (join $ sr <$> mrs)

data StepResult
  = StepResult Step [ Int ] [ Int ]

instance Show StepResult where
  show (StepResult _ [] (n : [])) =
    show n
  show (StepResult _ [] is) =
    "(" <> (sepWith " + " is) <> ")"
  show (StepResult _ es is) =
    "(" <> "[" <> sepWith ", " es <> "] " <> (sepWith " + " is) <> ")"

data CompoundRoll
  = CompoundRoll Times Roll

instance Show CompoundRoll where
  show (CompoundRoll (Times 1) s) =
    show s
  show (CompoundRoll (Times t) s) =
    (show t) <> " times " <> show s

data Roll =
  Roll Step [ (Modifier, Step) ]

instance Show Roll where
  show (Roll s []) =
    show s
  show (Roll s ss) =
    let
      showss (Plus, r) = " + " <> (show r)
      showss (Minus, r) = " - " <> (show r)
    in
      (show s) <> (join $ showss <$> ss)

data Step
  = Dice Times Sides DropKeep
  | Constant Int

instance Show Step where
  show (Dice (Times t) (Sides s) KeepAll) =
    (show t) <> "d" <> (show s)
  show (Dice (Times t) (Sides s) dk) =
    (show t) <> "d" <> (show s) <> " " <> (show dk)
  show (Constant c) = show c

data Modifier
 = Plus
 | Minus

instance Show Modifier where
 show Plus = "+"
 show Minus = "-"

data DropKeep
  = KeepAll
  | Drop HighLow Int
  | Keep HighLow Int

instance Show DropKeep where
  show KeepAll = "keep all"
  show (Drop Lowest 1) = "drop lowest"
  show (Keep Highest 1) = "keep highest"
  show (Drop h i) = "drop " <> (show h) <> " " <> (show i)
  show (Keep h i) = "keep " <> (show h) <> " " <> (show i)

data HighLow
  = Highest
  | Lowest

instance Show HighLow where
  show Highest = "highest"
  show Lowest = "lowest"

newtype Times = Times { getTimes :: Int } deriving (Eq, Ord)

instance Show Times where
  show (Times n) = (show n) <> " times"

newtype Sides = Sides { getSides :: Int } deriving (Eq, Ord, Show)

sepWith :: Show b => String -> [ b ] -> String
sepWith a bs =
  intercalate a (show <$> bs)
