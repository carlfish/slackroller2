{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}

module Server
    ( startApp
    ) where

import System.Exit (die)
import Network.Socket (setNonBlockIfNeeded, fdSocket)
import Network.Wai
import Network.Wai.Handler.Warp
import Data.Maybe (isJust)
import Servant
import qualified Slack as Slack
import Network.Wai.Middleware.RequestLogger
import Network.Socket.Activation (getActivatedSockets)
import System.ReadEnvVar

type API =
  "dice" :> "slack"  :> Slack.API

debug :: IO Bool
debug = pure True
--  isJust <$> (lookupEnv "DEBUG_MODE" :: IO (Maybe String))

settings :: Settings
settings = setPort 1234 defaultSettings

startApp :: IO ()
startApp =
  let
    runApp d =
      if d then logStdoutDev (app) else app
  in do
    d <- debug
    maybeSocks <- getActivatedSockets
    case maybeSocks of 
      Nothing  -> run 1234 $ runApp d
      Just [s] -> 
        do
          setNonBlockIfNeeded (fdSocket s)
          runSettingsSocket settings s $ runApp d
      Just ss  -> die ("Expected one configured socket, got " <> show (length ss))

app :: Application
app = serve api Slack.api

api :: Proxy API
api = Proxy
