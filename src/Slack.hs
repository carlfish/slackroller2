{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}

module Slack (API, api) where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson
import Data.Monoid ((<>))
import Data.Text (Text)
import Dice
import Servant
import Web.FormUrlEncoded (FromForm, fromForm, parseUnique)

type API
  =  ReqBody '[FormUrlEncoded] PostData
  :> Post '[JSON] SlackResponse

newtype UserId = UserId String
  deriving (Show)
newtype Cmd = Cmd Text
  deriving (Show)

data PostData = PostData Cmd UserId
  deriving (Show)

instance FromForm PostData where
  fromForm f = PostData
    <$> (Cmd <$> parseUnique "text" f)
    <*> (UserId <$> parseUnique "user_id" f)

data SlackResponse
  = SlackFail String
  | SlackSucc String [SlackResult]

instance ToJSON SlackResponse where
  toJSON (SlackFail text) =
    object
    [ "response_type" .= String "ephemeral"
    , "text" .= text ]
  toJSON (SlackSucc text atts) =
    object
      [ "response_type" .= String "in_channel"
      , "text" .= text
      , "attachments" .= atts ]

newtype SlackResult = SlackResult RollResult

instance ToJSON SlackResult where
  toJSON (SlackResult r) =
    let
      rolltext = (show r) <> " => " <> (show $ total r)
    in
      if (critical r) then
        object
          [ "color" .= (String "good")
          , "text" .= (rolltext <> " *critical!*")]
      else
        object [ "text" .= rolltext ]

api :: PostData -> Handler SlackResponse
api (PostData (Cmd cmd) userid)
  = (liftIO . toResponse) $ do
    r <- parse cmd
    res <- rollDice r
    pure (userid, (formatRolls r), res)

toResponse :: Either String (UserId, String, IO [ RollResult ]) -> IO SlackResponse
toResponse (Left err) =
  pure $ SlackFail err
toResponse (Right ((UserId sender), roll, result)) =
  (\r -> SlackSucc ("<@" <> sender <> "> rolled " <> roll <> ":") (SlackResult <$> r)) <$> result
