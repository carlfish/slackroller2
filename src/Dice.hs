module Dice
  ( module Dice.Types
  , critical
  , formatRolls
  , parse
  , rollDice
  , total
  ) where

import Data.Maybe (maybe)
import Control.Applicative ((<|>))
import Dice.Types
import Dice.Roll (rollAll)
import qualified Dice.Parser as P
import Data.Bifunctor (first)
import Data.List (intercalate)
import Data.Text (Text)
import System.Random (randomRIO)

parse :: Text -> Either String [ CompoundRoll ]
parse input = first (const "Unable to parse roll. For help, visit https://bitbucket.org/carlfish/slackroller2/src/master/README.md") (P.parse input)

rollDice :: [ CompoundRoll ] -> Either String (IO [ RollResult ])
rollDice rs =
  rollAll (\i -> randomRIO (1, i)) <$> (safeDecompose (Limits 20 30 1000) rs)

critical :: RollResult -> Bool
critical (RollResult h t) =
  criticalStep (Plus, h) || any criticalStep t

total :: RollResult -> Int
total (RollResult h t) =
  let
    modTotal (Plus, n) = stepTotal n
    modTotal (Minus, n) = negate (stepTotal n)
  in
    (stepTotal h) + sum (modTotal <$> t)

formatRolls :: [CompoundRoll] -> String
formatRolls rs = intercalate ", " (show <$> rs)

criticalStep :: (Modifier, StepResult) -> Bool
-- A step is critical iff it is a natural 20 on a single +ve d20 (after discards)
criticalStep (Plus, (StepResult (Dice _ (Sides 20) _) _ (20 : []))) =
  True
criticalStep _ =
  False

stepTotal :: StepResult -> Int
stepTotal (StepResult _ _ is) =
  sum is

safeDecompose :: Limits -> [ CompoundRoll ] -> Either String [ Roll ]
safeDecompose limits rs =
  let
    decomposed =
       (\(CompoundRoll (Times n) roll) -> (replicate n roll)) =<< rs
    test a b =
      if b then Just a else Nothing
    runTests
      =   test "Too many rolls"            (countRolls rs > rolls limits)
      <|> test "Too many dice in one roll" (any (> dicePerRoll limits) (countDice <$> decomposed))
      <|> test "Too many sides on a die"   (any (> sidesPerDie limits) (maxSides <$> decomposed))
  in
    maybe (Right decomposed) Left runTests

countRolls :: [ CompoundRoll ] -> Int
countRolls crs =
  let
    times (CompoundRoll (Times n) _) = n
  in
    sum (times <$> crs)

countDice :: Roll -> Int
countDice (Roll h t) =
  let
    cnt (Constant _) = 0
    cnt (Dice (Times n) _ _) = n
  in
    (cnt h) + foldl (\c (_, r) -> c + (cnt r)) 0 t

maxSides :: Roll -> Int
maxSides (Roll h t) =
  let
    sides (Constant _) = 0
    sides (Dice _ (Sides n) _) = n
    tsides (_, s) = sides s
  in
    maximum (sides h : (tsides <$> t))
