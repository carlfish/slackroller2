# Usage: #

## Slack (slash command): ##

Responds to the configured slash command. Parse errors will be quietly reported
to the requester without bothering the rest of the channel

For example, if configured against the /roll command:

    /roll 2d20 drop 1 + 4, 1d8 + 2

## Stride (bot, experimental): ##

When the bot is installed in your conversation, invoke it by mentioning it:

    @roll advantage + 1d4
    
The bot will respond in the conversation it was mentioned. Messaging the bot directly is currently not supported.

## Supported syntax: ##

(While spaces are allowed, and used in the examples, they are often optional.)

### Simple rolls ####

* d10 -> roll ten-sided die.
* 2d20 -> roll two twenty-sided dice, add them together
* 2d10 + 10 -> add a constant to a roll (similarly: 2d10 - 10)
* 2d10 + 3d6 -> add multiple rolls together
* 2d10, 3d6 -> make multiple rolls separately but don't add them.
* 6 times 3d6 -> make the same roll multiple times without adding them (synonym: 6x3d6)

### Dropping and Keeping ###

* 4d6 drop lowest 1 -> roll 4d6, drop the lowest roll and sum the remainder
* 4d6 keep highest 3 -> roll 4d6, sum the highest three rolls (this is the same as 4d6 drop 1)

The following work analogously to the above, and are synonymous:

* 8d6 drop lowest 2 = 8d6 drop 2 = 8d6 dl 2 = 8d6 d 2
* 8d6 drop highest 2 = 8d6 dh 2
* 8d6 keep lowest 2 = 8d6 kl 2
* 8d6 keep highest 2 = 8d6 keep 2 = 8d6 kh 2 = 8d6 k 2

If you are only dropping/keeping a single die, the number can be omitted:

* 2d20 drop lowest 1 = 2d20 drop lowest = 2d20 dl = 2d20 d
* 2d20 drop highest 1 = 2d20 drop highest = 2d20 dh
* 2d20 keep lowest 1 = 2d20 keep lowest = 2d20 kl
* 2d20 keep highest 1 = 2d20 keep highest = 2d20 kh = 2d20 k

So, for example, this will roll standard d20 ability scores for a new character:

    /roll 6 times 4d6 drop 1

### Synonyms ###

* advantage (or adv) -> synonymous with 2d20 keep highest
* disadvantage (or dis) -> synonymous with 2d20 keep lowest

These synonyms can be used in rolls wherever their long form can be used, so the following will work as expected:

    /roll adv + 1d4 + 3